Shard Takeover is a tool that can be used to simulate different security scenarios relevant
to Shardus. The results of these simulations give an idea of how robust the security is.

The following are the commands that the tool supports.

# malicious-shard

`malicious-shard` command is used to simulate random shard assignment given the
presence of a significant proportion of malicious nodes, to see if a shard might
wind up with a majority of malicious nodes. This checks all possible shards
made up of consecutive nodes (placed in a circle) after randomly assigning a
fixed percentage of nodes to be malicious, so the total number of checked shards
is equal to the number of nodes.

It also calculates the theoretical probability for a shard to have a majority of
malicious nodes in two ways. First, it calculates this when each node is
independently malicious with the given percentage odds (this is independent of
the number of nodes). It also calculates this when the total number of
malicious nodes is fixed, as specified by the percentage and the total number of
nodes.

The program takes 7 arguments:

- -n - the number of active nodes
- -s - the shard size
- -h - the number of honest nodes
- -m - the number of malicious nodes
- -f - the failure threshold number in a shard
- -t - the number of simulations to perform
- -p - the number of times to show the progress bar

An example run of the program looks like this:

```
node build/src/index.js malicious-shard -n 1000 -s 128 -h 3300 -m 1700 -f 65 -t 100000
```

# failure-time

`failure-time` command is used to simulate an attacker that is able to
control a certain percentage of nodes on standby, where we run a simulation
until a shard fails, and then we record the number of rotations it took.

The program takes 8 arguments:

- -n - the number of active nodes
- -s - the shard size
- -h - the number of honest nodes
- -m - the number of malicious nodes
- -f - the number of malicious nodes in a shard that indicates failure
- -t - the total amount of time to run simulations
- -r - the number of nodes to rotate out on each cycle
- -p - the number of times to print a progress bar

An example run of the program looks like this:

```
node build/src/index.js failure-time -n 1000 -s 128 -h 5000 -m 2500 -f 65 -r 2 -t 100000
```

# standby-attack

`standby-attack` command is used to simulate an attacker that is able to
control a certain percentage of nodes on standby, which gives them a
greater percentage of joining the network, due to the deterministic
nature of the rotation process.

A single simulation here runs until the number of rotations is double
the size of the network, which should be enough to maximize the chances
of the attacker getting a majority of nodes in a shard.

The program takes 8 arguments:

- -n - the number of nodes to simulate
- -s - the shard size
- -h - the number of honest nodes
- -m - the number of malicious nodes added to the standby list
- -f - the percentage of nodes in a shard that indicates failure
- -t - the number of simulations to perform
- -r - the number of nodes to rotate out on each cycle
- -p - the number of times to print a progress bar

An example run of the program looks like this:

```
node build/src/index.js standby-attack -n 1000 -s 128 -h 5000 -m 2000 -f 65 -r 2 -t 50
```

# node-drift

`node-drift` command is used to simulate the drift of nodes in the network.
This is done by simulating the rotation of nodes in the network, and
recording how much the position of each node has changed.

A single simulation here runs until the number of rotations specified has
been performed, and the statistics are recorded in several buckets.

The program takes 5 arguments:

- -n - the number of active nodes
- -s - the shard size
- -t - the number of simulations to perform
- -r - the number of nodes to rotate out on each cycle
- -p - the number of times to print a progress bar

An example run of the program looks like this:

```
node build/src/index.js node-drift -n 1000 -s 128 -r 2 -t 10000
```

# challenge-cover

`challenge-cover` command simulates random shard formation given a certain percentage of malicious
nodes, to see if each shard has any malicious nodes.

The program takes 7 arguments:

- -n - the number of active nodes
- -s - the number of nodes per shard
- -h - the number of honest nodes
- -m - the number of malicious nodes
- -f - the number of shards with malicious nodes that constitutes failure
- -t - the number of times to run the simulation
- -p - the number of progress updates to show while running the simulation

An example run of the program looks like this:

```
node build/src/index.js challenge-cover -n 1000 -s 128 -h 4000 -m 100 -f 1000 -t 10000
```

# cover-rotation

'cover-rotation' simulates random shard formation given a certain percentage of malicious
nodes, to see how many shards have a malicious node. Then, if there is no
immediate failure, it will perform rotation until the number falls below a threshold.

The program takes 8 arguments:

- -n - the number of active nodes
- -s - the number of nodes per shard
- -h - the number of honest nodes
- -m - the number of malicious nodes
- -f - the number of shards with malicious nodes that constitutes failure
- -t - the number of times to run the simulation
- -r - the number of nodes to rotate out on each cycle
- -p - the number of progress updates to show while running the simulation

An example run of the program looks like this:

```
node build/src/index.js cover-rotation -n 1000 -s 128 -h 4000 -m 100 -f 1000 -r 2 -t 100000
```

# cycle-marker

'cycle-marker' simulates malicious nodes capable of trying different cycle markers so as to
get high values when hashed with several malicious node IDs. It will compute different
relevant statistics.

The program takes 6 arguments:

- -n - the number of active nodes
- -m - the number of active malicious nodes
- -v - the number of votes needed for a marker to be a viable candidate
- -h - the number of active honest node votes to generate
- -t - the total number of forging simulations to do
- -p - the number of progress updates to show while running the simulation

An example run of the program looks like this:

```
node build/src/index.js cycle-marker -n 1000 -m 50 -v 3 -h 10 -t 10000
```
