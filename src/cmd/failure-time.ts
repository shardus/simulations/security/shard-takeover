import {
  computeMaliciousShardGivenProbability,
  computeMaliciousShardGivenCounts,
  validateNodesArgument,
  validateShardSizeArgument,
  validateMaliciousNodesArguments,
  validateFailureSizeArguments,
  validateNumberOfSimulations,
  validateHonestNodesArgument,
  validateRotateNodesArgument,
  validateProgressCountArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestFailureTime simulates random shard formation given a certain percentage of malicious
 * nodes, to see if any shard has a majority of malicious nodes.  Then, if there is no
 * immediate failure, it will perform rotation until a shard fails.  It will also compute the
 * theoretical probability for a single shard in two ways:
 *
 * 1. Assuming each node is independently malicious or not, with the specified probability
 * 2. Assuming the percentage reflects the actual number of malicious nodes (this should be
 *    slightly lower)
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -h, The number of honest nodes.
 * @param {number} -m, The number of malicious nodes.
 * @param {number} -f, The number of malicious nodes in a shard that constitutes failure.
 * @param {number} -t, The number of times to run the simulation.
 * @param {number} -r, The number of nodes to rotate out on each cycle.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestFailureTime(options: any) {
  const [
    nodeCount,
    shardSize,
    honestCount,
    maliciousCount,
    failureSize,
    noOfCycles,
    noToRotate,
    progressCount,
  ] = validateFailureTimeArguments(options)

  // Some global initializations.
  let totalNodeCount = honestCount + maliciousCount

  if (nodeCount > totalNodeCount) {
    console.log('Error: Number of active nodes cannot be more than total nodes.')
    return
  }

  let maliciousProbability = maliciousCount / totalNodeCount
  let maliciousPercentage = maliciousProbability * 100

  console.log('Active nodes:    ' + nodeCount)
  console.log('Total nodes:     ' + totalNodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Malicious pct:   ' + maliciousPercentage.toFixed(2))
  console.log('Malicious count: ' + maliciousCount)
  console.log('Failure size:    ' + failureSize)
  console.log('Nodes to rotate: ' + noToRotate)

  computeMaliciousShardGivenProbability(shardSize, maliciousProbability, failureSize)
  computeMaliciousShardGivenCounts(shardSize, totalNodeCount, maliciousCount, failureSize)

  let initialFailureCount = 0
  let sims = 0
  let cycles = 0
  let rotationSimulationCount = 0
  let totalRotationCount = 0
  let timeLimitReached = false

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  // start the simulation and run for `noOfSimulations` of times.
  while (cycles < noOfCycles) {
    // count the time to build the initial network
    cycles += 1
    sims += 1

    // display progress bar
    if (cycles > (progressBar / 100) * noOfCycles) {
      while (cycles > (progressBar / 100) * noOfCycles) {
        progressBar += progressInc
      }
      console.log('Percent done: ' + (progressBar - progressInc))
    }

    // build the initial network
    let nodeList = new NodeList(nodeCount, shardSize, failureSize, maliciousCount, totalNodeCount)

    // immediately fail if any shard has a malicious majority
    if (nodeList.getMaliciousShardCount() > 0) {
      initialFailureCount += 1
    } else {
      // perform rotation until some shard has a malicious majority
      let maliciousMajority = false

      while (!maliciousMajority && !timeLimitReached) {
        // remove oldest nodes and add new ones
        nodeList.cycleRotation(noToRotate)

        if (nodeList.getMaliciousShardCount() > 0) {
          maliciousMajority = true
        } else if (nodeList.rotationCount >= noOfCycles * noToRotate) {
          timeLimitReached = true
        }

        cycles += 1
      }

      if (maliciousMajority) {
        rotationSimulationCount += 1
        totalRotationCount += nodeList.rotationCount
      }
    }
  }

  console.log('Simulations run:          ' + sims)
  console.log('Immediate failed runs:    ' + initialFailureCount)
  if (initialFailureCount > 0) {
    console.log('  Pct immed failed runs:  ' + ((100 * initialFailureCount) / sims).toFixed(2))
  }
  if (rotationSimulationCount > 0) {
    console.log('Avg rots to failure:      ' + (totalRotationCount / rotationSimulationCount).toFixed(2))
    console.log(
      'Avg cycles to failure:    ' + (totalRotationCount / (rotationSimulationCount * noToRotate)).toFixed(2)
    )
    console.log(
      'Pct chance of cycle fail: ' +
        (100 * ((rotationSimulationCount * noToRotate) / totalRotationCount)).toFixed(8)
    )
  }

  if (timeLimitReached) {
    console.log('Reached simulation limit on last run after ' + noOfCycles + ' cycles.')
  }
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateFailureTimeArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const honestCount = validateHonestNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const failureSize = validateFailureSizeArguments(options)
  const cycles = validateNumberOfSimulations(options)
  const rotateNodes = validateRotateNodesArgument(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, honestCount, maliciousCount, failureSize, cycles, rotateNodes, progressCount]
}
