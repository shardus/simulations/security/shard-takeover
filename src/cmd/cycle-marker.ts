import {
  validateNodesArgument,
  validateMaliciousNodesArguments,
  validateNumberOfSimulations,
  validateProgressCountArgument,
  validateVoteCountArguments,
  validateHonestNodesArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestCycleMarker simulates malicious nodes capable of trying different cycle markers so as to
 * get high values when hashed with several malicious node IDs. It will compute different
 * relevant statistics.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of nodes to simulate.
 * @param {number} -m, The number of malicious nodes.
 * @param {number} -v, The number of votes retained for a cycle marker.
 * @param {number} -h, The number of active honest node votes to generate.
 * @param {number} -t, The total number of simulations to run.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestCycleMarker(options: any) {
  const [nodeCount, maliciousCount, voteCount, honestSims, maliciousSims, progressCount] =
    validateCycleMarkerArguments(options)

  // Some global initializations.
  let honestCount = nodeCount - maliciousCount

  if (honestCount < voteCount || maliciousCount < voteCount) {
    console.log(
      'The number of honest and malicious nodes must each be at least the retained vote count: ' +
        voteCount +
        '.'
    )
    return
  }

  console.log('Total nodes:     ' + nodeCount)
  console.log('Honest nodes:    ' + honestCount)
  console.log('Malicious nodes: ' + maliciousCount)
  console.log('Votes retained:  ' + voteCount)

  console.log('Generating honest votes...')

  let progressInc = 100 / progressCount
  let progressBar = progressInc

  // make an array to store the honest vote scores
  let honestVoteScores = new Array(honestSims)

  for (let simulations = 0; simulations < honestSims; simulations++) {
    // display progress bar
    if (simulations > (progressBar / 100) * honestSims) {
      while (simulations > (progressBar / 100) * honestSims) {
        progressBar += progressInc
      }
      console.log('Percent done: ' + (progressBar - progressInc))
    }

    // build the initial network
    let honestNodeList = new NodeList(honestCount, 0, 0, 0, nodeCount)

    // get the honest vote score
    let honestVoteScore = honestNodeList.getVoteScore(voteCount)
    honestVoteScores[simulations] = honestVoteScore
  }

  // sort the honest vote scores in descending order
  honestVoteScores.sort((a, b) => b - a)

  console.log('Generating forged votes...')
  // make an array with the same size as the number of honest simulations
  let maliciousVoteScores = new Array(maliciousSims)

  progressBar = progressInc

  for (let simulations = 0; simulations < maliciousSims; simulations++) {
    // display progress bar
    if (simulations > (progressBar / 100) * maliciousSims) {
      while (simulations > (progressBar / 100) * maliciousSims) {
        progressBar += progressInc
      }
      console.log('Percent done: ' + (progressBar - progressInc))
    }

    // build the initial network
    let maliciousNodeList = new NodeList(maliciousCount, 0, 0, 0, nodeCount)

    // get the malicious vote rank
    let maliciousVoteScore = maliciousNodeList.getVoteScore(voteCount)
    maliciousVoteScores[simulations] = maliciousVoteScore
  }

  // sort the malicious vote scores in descending order
  maliciousVoteScores.sort((a, b) => b - a)

  // add a value at the end of the honest vote scores to make the statistics easier to compute
  honestVoteScores.push(0)

  // get the aggregate ranks
  let honestRank = 0
  let lastIndex = 0
  let maliciousVoteRanks = new Array(honestSims + 1).fill(0)

  for (let i = 0; i < maliciousSims; i++) {
    while (honestVoteScores[honestRank] > maliciousVoteScores[i]) {
      maliciousVoteRanks[honestRank] = i - lastIndex
      lastIndex = i
      honestRank += 1
    }
  }

  maliciousVoteRanks[honestRank] = maliciousSims - lastIndex

  // compute the statistics
  let minimumTime = maliciousSims
  let averageTime = 0
  let averageProbability = 0
  let cumulativeCount = 0

  for (let i = honestSims; i > 0; i--) {
    cumulativeCount += maliciousVoteRanks[i]
    averageTime += maliciousSims / Math.max(1, cumulativeCount)
    averageProbability += cumulativeCount / honestSims
  }

  minimumTime = maliciousSims / Math.max(1, cumulativeCount)

  // print the results
  console.log('Honest votes generated:  ' + honestSims)
  console.log('Forged votes generated:  ' + maliciousSims)
  console.log('Minimum time to forge:   ' + minimumTime.toFixed(2))
  console.log('Mean time to forge:      ' + (averageTime / honestSims).toFixed(2))
  console.log('Harmonic time to forge:  ' + (maliciousSims / averageProbability).toFixed(2))
  console.log('Probability of forging:  ' + (averageProbability / maliciousSims).toFixed(8))
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateCycleMarkerArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const voteCount = validateVoteCountArguments(options)
  const honestSims = validateHonestNodesArgument(options)
  const maliciousSims = validateNumberOfSimulations(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, maliciousCount, voteCount, honestSims, maliciousSims, progressCount]
}
