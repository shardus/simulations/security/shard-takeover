import {
  computeMaliciousShardGivenProbability,
  computeMaliciousShardGivenCounts,
  validateNodesArgument,
  validateShardSizeArgument,
  validateMaliciousNodesArguments,
  validateFailureSizeArguments,
  validateNumberOfSimulations,
  validateHonestNodesArgument,
  validateProgressCountArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestMaliciousShard simulates random shard formation given a certain percentage of malicious
 * nodes, to see if any shard has a majority of malicious nodes.  It will also compute the
 * theoretical probability for a single shard in two ways:
 *
 * 1. Assuming each node is independently malicious or not, with the specified probability
 * 2. Assuming the percentage reflects the actual number of malicious nodes (this should be
 *    slightly lower)
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -h, The number of honest nodes.
 * @param {number} -m, The number of malicious nodes.
 * @param {number} -f, The number of malicious nodes in a shard that constitutes failure.
 * @param {number} -t, The number of times to run the simulation.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestMaliciousShard(options: any) {
  const [nodeCount, shardSize, honestCount, maliciousCount, failureSize, noOfSimulations, progressCount] =
    validateMaliciousShardArguments(options)

  // Some global initializations.
  let totalNodeCount = honestCount + maliciousCount

  if (totalNodeCount < nodeCount) {
    console.log('Total node count is less than the active node count')
    return
  }

  let maliciousPercentage = (100 * maliciousCount) / totalNodeCount
  let maliciousProbability = maliciousPercentage / 100

  console.log('Active nodes:    ' + nodeCount)
  console.log('Total nodes:     ' + totalNodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Malicious pct:   ' + maliciousPercentage.toFixed(2))
  console.log('Malicious count: ' + maliciousCount)
  console.log('Failure size:    ' + failureSize)

  computeMaliciousShardGivenProbability(shardSize, maliciousProbability, failureSize)
  computeMaliciousShardGivenCounts(shardSize, totalNodeCount, maliciousCount, failureSize)

  let failedSimulationCount = 0
  let maliciousShardCount = 0
  let maxMaliciousSum = 0

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  // start the simulation and run for `noOfSimulations` of times.
  for (let sims = 0; sims < noOfSimulations; sims++) {
    if (sims > (progressBar / 100) * noOfSimulations) {
      console.log('Percent done: ' + progressBar)
      progressBar += progressInc
    }

    // build the initial network
    let nodeList = new NodeList(nodeCount, shardSize, failureSize, maliciousCount, totalNodeCount)

    maxMaliciousSum += nodeList.getMaxMaliciousInShard()

    if (nodeList.getMaliciousShardCount() > 0) {
      failedSimulationCount += 1
      maliciousShardCount += nodeList.getMaliciousShardCount()
    }
  }

  console.log('Simulations run:          ' + noOfSimulations)
  console.log(
    'Avg max malicious (pct):  ' + (((maxMaliciousSum / noOfSimulations) * 100) / shardSize).toFixed(2)
  )
  console.log(' in shard       (count):  ' + (maxMaliciousSum / noOfSimulations).toFixed(2))
  console.log('Failed runs:    ' + failedSimulationCount)
  if (failedSimulationCount > 0) {
    console.log('  Percentage failed runs: ' + (100 * failedSimulationCount) / noOfSimulations)
    console.log('  Malicious shard pct:    ' + (100 * maliciousShardCount) / (nodeCount * noOfSimulations))
    console.log('  On fail, mal shard avg: ' + (maliciousShardCount / failedSimulationCount).toFixed(4))
  }
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateMaliciousShardArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const honestCount = validateHonestNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const failureSize = validateFailureSizeArguments(options)
  const simulations = validateNumberOfSimulations(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, honestCount, maliciousCount, failureSize, simulations, progressCount]
}
