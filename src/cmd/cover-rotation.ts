import {
  validateNodesArgument,
  validateShardSizeArgument,
  validateMaliciousNodesArguments,
  validateFailureSizeArguments,
  validateNumberOfSimulations,
  validateHonestNodesArgument,
  validateRotateNodesArgument,
  validateProgressCountArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestCoverRotation simulates random shard formation given a certain percentage of malicious
 * nodes, to see how many shards have a malicious node.  Then, it will perform rotation for the
 * specified number of cycles and collect statistics.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -h, The number of honest nodes.
 * @param {number} -m, The number of malicious nodes.
 * @param {number} -f, The number of shards with malicious nodes that constitutes failure.
 * @param {number} -t, The number of times to run the simulation.
 * @param {number} -r, The number of nodes to rotate out on each cycle.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestCoverRotation(options: any) {
  const [
    nodeCount,
    shardSize,
    honestCount,
    maliciousCount,
    failureCount,
    noOfCycles,
    noToRotate,
    progressCount,
  ] = validateCoverRotationArguments(options)

  // Some global initializations.
  let totalNodeCount = honestCount + maliciousCount

  if (nodeCount > totalNodeCount) {
    console.log('Error: Number of active nodes cannot be more than total nodes.')
    return
  }

  let maliciousProbability = maliciousCount / totalNodeCount
  let maliciousPercentage = maliciousProbability * 100

  let failureSize = 1

  console.log('Active nodes:    ' + nodeCount)
  console.log('Total nodes:     ' + totalNodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Malicious pct:   ' + maliciousPercentage.toFixed(2))
  console.log('Malicious count: ' + maliciousCount)
  console.log('Failure thresh:  ' + failureCount)
  console.log('Nodes to rotate: ' + noToRotate)

  let cycles = 0

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  // build the initial network
  let nodeList = new NodeList(nodeCount, shardSize, failureSize, maliciousCount, totalNodeCount)

  let cover = nodeList.getMaliciousShardCount()
  let maximumCover = cover
  let minimumCover = cover
  let averageCover = cover
  let thresholdCover = 0
  if (cover >= failureCount) {
    thresholdCover += 1
  }

  // start the simulation and run for `noOfSimulations` of times.
  while (cycles < noOfCycles) {
    // count the time to build the initial network
    cycles += 1

    // display progress bar
    if (cycles > (progressBar / 100) * noOfCycles) {
      while (cycles > (progressBar / 100) * noOfCycles) {
        progressBar += progressInc
      }
      console.log('Percent done: ' + (progressBar - progressInc))
    }

    // remove oldest nodes and add new ones
    nodeList.cycleRotation(noToRotate)

    // adjust the cover values
    let cover = nodeList.getMaliciousShardCount()
    if (cover > maximumCover) {
      maximumCover = cover
    }
    if (cover < minimumCover) {
      minimumCover = cover
    }
    averageCover += cover

    if (cover >= failureCount) {
      thresholdCover += 1
    }
  }

  // print the results
  console.log('Cycles simulated:          ' + noOfCycles)
  console.log('Maximum cover:             ' + maximumCover)
  console.log('Minimum cover:             ' + minimumCover)
  console.log('Average cover:             ' + (averageCover / noOfCycles).toFixed(2))
  console.log('Cycles at threshold cover: ' + thresholdCover)
  console.log('Pct cycles at threshold:   ' + ((100 * thresholdCover) / noOfCycles).toFixed(2))
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateCoverRotationArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const honestCount = validateHonestNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const failureCount = validateFailureSizeArguments(options)
  const cycles = validateNumberOfSimulations(options)
  const rotateNodes = validateRotateNodesArgument(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, honestCount, maliciousCount, failureCount, cycles, rotateNodes, progressCount]
}
