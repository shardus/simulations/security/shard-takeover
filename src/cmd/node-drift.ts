import {
  validateNodesArgument,
  validateShardSizeArgument,
  validateNumberOfSimulations,
  validateRotateNodesArgument,
  validateProgressCountArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestNodeDrift command is used to simulate the drift of nodes in the network.
 * This is done by simulating the rotation of nodes in the network, and
 * recording how much the position of each node has changed.
 *
 * A single simulation here runs until the number of rotations specified has
 * been performed, and the statistics are recorded in several buckets.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -t, The number of cycles to run the simulation.
 * @param {number} -r, The number of nodes to rotate out on each cycle.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestNodeDrift(options: any) {
  const [nodeCount, shardSize, noOfCycles, noToRotate, progressCount] = validateNodeDriftArguments(options)

  // Some global initializations.

  console.log('Active nodes:    ' + nodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Nodes to rotate: ' + noToRotate)

  let cycles = 0

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  let nodeList = new NodeList(nodeCount, shardSize, 0, 0, nodeCount)

  // start the simulation and run for `noOfSimulations` of times.
  while (cycles < noOfCycles) {
    // count the time to build the initial network
    cycles += 1

    // display progress bar
    if (cycles > (progressBar / 100) * noOfCycles) {
      while (cycles > (progressBar / 100) * noOfCycles) {
        progressBar += progressInc
      }
      console.log('Percent done: ' + (progressBar - progressInc))
    }

    // remove oldest nodes and add new ones
    nodeList.cycleRotation(noToRotate)

    cycles += 1
  }

  // collect the drift data
  nodeList.computeNodeDriftStats()

  for (let i = 0; i < nodeList.averageDrift.length; i++) {
    console.log('Bucket ' + i + ':')
    console.log(' Avg drift: ' + nodeList.averageDrift[i].toFixed(2))
    console.log(' Max drift: ' + nodeList.maxDrift[i])
  }
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateNodeDriftArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const cycles = validateNumberOfSimulations(options)
  const rotateNodes = validateRotateNodesArgument(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, cycles, rotateNodes, progressCount]
}
