import {
  validateNodesArgument,
  validateShardSizeArgument,
  validateMaliciousNodesArguments,
  validateFailureSizeArguments,
  validateNumberOfSimulations,
  validateHonestNodesArgument,
  validateProgressCountArgument,
  computeMaliciousShardGivenProbability,
  computeMaliciousShardGivenCounts,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestChallengeCover simulates random shard formation given a certain percentage of malicious
 * nodes, to see if each shard has any malicious nodes.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -h, The number of honest nodes.
 * @param {number} -m, The number of malicious nodes.
 * @param {number} -f, The percentage of shards with malicious nodes that constitutes failure.
 * @param {number} -t, The number of times to run the simulation.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestChallengeCover(options: any) {
  const [nodeCount, shardSize, honestCount, maliciousCount, failureCount, noOfSimulations, progressCount] =
    validateChallengeCoverArguments(options)

  // Some global initializations.
  let totalNodeCount = honestCount + maliciousCount

  if (totalNodeCount < nodeCount) {
    console.log('Total node count is less than the active node count')
    return
  }

  let maliciousProbability = maliciousCount / totalNodeCount
  let maliciousPercentage = 100 * maliciousProbability

  // just need 1 node in a shard to consider a shard a "failure" in this simulation
  let failureSize = 1

  console.log('Active nodes:    ' + nodeCount)
  console.log('Total nodes:     ' + totalNodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Malicious pct:   ' + maliciousPercentage.toFixed(2))
  console.log('Malicious count: ' + maliciousCount)
  console.log('Failure thresh:  ' + failureCount)

  // compute the theoretical odds of a shard having no malicious node
  computeMaliciousShardGivenProbability(shardSize, maliciousProbability, failureSize)
  computeMaliciousShardGivenCounts(shardSize, totalNodeCount, maliciousCount, failureSize)

  let failedSimulationCount = 0
  let coveredShardCount = 0
  let maximumCover = 0
  let minimumCover = nodeCount

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  // start the simulation and run for `noOfSimulations` of times.
  for (let sims = 0; sims < noOfSimulations; sims++) {
    if (sims > (progressBar / 100) * noOfSimulations) {
      console.log('Percent done: ' + progressBar)
      progressBar += progressInc
    }

    // build the initial network
    let nodeList = new NodeList(nodeCount, shardSize, failureSize, maliciousCount, totalNodeCount)

    let coveredShards = nodeList.getMaliciousShardCount()
    coveredShardCount += coveredShards

    if (coveredShards > maximumCover) {
      maximumCover = coveredShards
    }

    if (coveredShards < minimumCover) {
      minimumCover = coveredShards
    }

    if (coveredShards >= failureCount) {
      failedSimulationCount += 1
    }
  }

  console.log('Simulations run:          ' + noOfSimulations)
  console.log('Avg coverage:   ' + (coveredShardCount / noOfSimulations).toFixed(2))
  console.log('Max coverage:   ' + maximumCover)
  console.log('Min coverage:   ' + minimumCover)
  console.log('Failed runs:    ' + failedSimulationCount)
  if (failedSimulationCount > 0) {
    console.log('  Percentage failed runs: ' + (100 * failedSimulationCount) / noOfSimulations)
  }
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateChallengeCoverArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const honestCount = validateHonestNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const failureCount = validateFailureSizeArguments(options)
  const simulations = validateNumberOfSimulations(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, honestCount, maliciousCount, failureCount, simulations, progressCount]
}
