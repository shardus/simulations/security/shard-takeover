import {
  validateNodesArgument,
  validateShardSizeArgument,
  validateMaliciousNodesArguments,
  validateFailureSizeArguments,
  validateNumberOfSimulations,
  validateHonestNodesArgument,
  validateRotateNodesArgument,
  validateProgressCountArgument,
} from '../network/utils'

import NodeList from '../network/nodeList'

/* TestStandbyAttack simulates random shard formation given a certain number of malicious
 * nodes, to see if any shard gets a majority of malicious nodes.  In particular, the network
 * starts with no malicious nodes, which increases the odds of a malicious node being added
 * to the network above the overall percentage of malicious nodes.  Then, we perform rotation
 * until a shard fails, or the advantage of the malicious nodes is lost.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * @param {number} -n, The number of active nodes.
 * @param {number} -s, The number of nodes per shard.
 * @param {number} -h, The number of honest nodes.
 * @param {number} -m, The number of malicious nodes added to standby.
 * @param {number} -f, The percentage of malicious nodes in a shard that constitutes failure.
 * @param {number} -t, The number of times to run the simulation.
 * @param {number} -r, The number of nodes to rotate out on each cycle.
 * @param {number} -p, The number of progress updates to show while running the simulation.
 */
export function TestStandbyAttack(options: any) {
  const [
    nodeCount,
    shardSize,
    honestCount,
    maliciousCount,
    failureSize,
    noOfSimulations,
    noToRotate,
    progressCount,
  ] = validateStandbyAttackArguments(options)

  // Some global initializations.
  let totalNodeCount = honestCount + maliciousCount

  if (nodeCount > honestCount) {
    console.log('Error: Number of active nodes cannot be more than honest nodes.')
    return
  }

  let maliciousPercentage = (100 * maliciousCount) / totalNodeCount

  console.log('Active nodes:    ' + nodeCount)
  console.log('Total nodes:     ' + totalNodeCount)
  console.log('Shard size:      ' + shardSize)
  console.log('Malicious pct:   ' + maliciousPercentage.toFixed(2))
  console.log('Malicious count: ' + maliciousCount)
  console.log('Failure size:    ' + failureSize)
  console.log('Nodes to rotate: ' + noToRotate)

  let failedSimulationCount = 0
  let totalRotationCount = 0

  console.log('Running simulations...')

  let progressBar = 100 / progressCount
  let progressInc = progressBar

  // start the simulation and run for `noOfSimulations` of times.
  for (let sims = 0; sims < noOfSimulations; sims++) {
    if (sims > (progressBar / 100) * noOfSimulations) {
      console.log('Percent done: ' + progressBar)
      progressBar += progressInc
    }

    // build the initial network (without any malicious nodes)
    let nodeList = new NodeList(nodeCount, shardSize, failureSize, 0, totalNodeCount)
    nodeList.maliciousCount = maliciousCount

    // perform rotation until some shard has a malicious majority
    let maliciousMajority = false

    while (!maliciousMajority && nodeList.rotationCount < 2 * nodeCount) {
      nodeList.cycleRotation(noToRotate)

      if (nodeList.getMaliciousShardCount() > 0) {
        maliciousMajority = true
      }
    }

    if (maliciousMajority) {
      failedSimulationCount += 1
      totalRotationCount += nodeList.rotationCount
    }
  }

  console.log('Simulations run:          ' + noOfSimulations)
  console.log('Failed runs:              ' + failedSimulationCount)
  console.log('Pct failed runs:          ' + (100 * failedSimulationCount) / noOfSimulations)
  console.log('Avg rots when failed:     ' + (totalRotationCount / failedSimulationCount).toFixed(2))
  console.log(
    'Avg cycles when failed:   ' + (totalRotationCount / (failedSimulationCount * noToRotate)).toFixed(2)
  )
}

/* validateMaliciousShardArguments checks if all the arguments are present and are proper number.
 *
 * @param {any} options, the commander data structure which has all the parsed arguments.
 */
function validateStandbyAttackArguments(options: any): number[] {
  const nodes = validateNodesArgument(options)
  const shardSize = validateShardSizeArgument(options)
  const honestCount = validateHonestNodesArgument(options)
  const maliciousCount = validateMaliciousNodesArguments(options)
  const failureSize = validateFailureSizeArguments(options)
  const simulations = validateNumberOfSimulations(options)
  const rotateNodes = validateRotateNodesArgument(options)
  const progressCount = validateProgressCountArgument(options)

  return [nodes, shardSize, honestCount, maliciousCount, failureSize, simulations, rotateNodes, progressCount]
}
