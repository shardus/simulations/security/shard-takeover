/* computeMaliciousShardGivenProbability finds the probability a shard will have a malicious majority
 * given the shard size and the probability that any node is malicious.
 *
 * @param {number} - shardSize
 * @param {number} - maliciousProbability
 */
export function computeMaliciousShardGivenProbability(
  shardSize: number,
  maliciousProbability: number,
  failureSize: number
): number {
  let honestProbability = 1 - maliciousProbability
  let honestRatio = honestProbability / maliciousProbability

  // efficiently calculate p^i (1-p)^(s-i) C(s,i) starting with p^s
  let specificCountOdds = Math.pow(maliciousProbability, shardSize)
  let accumulatedOdds = specificCountOdds

  for (let i = shardSize; i > failureSize; i--) {
    specificCountOdds = (specificCountOdds * honestRatio * i) / (shardSize + 1 - i)
    accumulatedOdds += specificCountOdds
  }

  console.log('Malicious shard probability (indep nodes, pct): ' + (100 * accumulatedOdds).toFixed(8))

  return accumulatedOdds
}

/* computeMaliciousShardGivenCounts finds the probability a shard will have a malicious majority
 * given the shard size, the total node count, and the malicious node count.
 *
 * @param {number} - shardSize
 * @param {number} - nodeCount
 * @param {number} - maliciousCount
 */
export function computeMaliciousShardGivenCounts(
  shardSize: number,
  nodeCount: number,
  maliciousCount: number,
  failureSize: number
): number {
  let honestCount = nodeCount - maliciousCount

  // efficiently calculate C(m,i) C(h,s-i) starting with C(m,s)
  // first compute C(m,s) and C(n,s)
  let specificCount = 1
  let totalCount = 1

  for (let i = 0; i < shardSize; i++) {
    specificCount = (specificCount * (maliciousCount - i)) / (i + 1)
    totalCount = (totalCount * (nodeCount - i)) / (i + 1)
  }

  let startingCount = shardSize
  // if malicious count is less than the shard size, different starting point
  if (maliciousCount < shardSize) {
    // compute C(h,s-m)
    specificCount = 1

    for (let i = 0; i < shardSize - maliciousCount; i++) {
      specificCount = (specificCount * (honestCount - i)) / (i + 1)
    }

    startingCount = maliciousCount
  }
  let accumulatedCount = specificCount

  for (let i = startingCount; i > failureSize; i--) {
    // going from C(m,i) C(h,s-i) to C(m,i-1) C(h,s+1-i)
    specificCount = (specificCount * i) / (shardSize + 1 - i)
    specificCount = (specificCount * (honestCount - shardSize + i)) / (maliciousCount + 1 - i)
    accumulatedCount += specificCount
  }

  console.log(
    'Malicious shard probability (fixed count, pct): ' + (100 * (accumulatedCount / totalCount)).toFixed(8)
  )

  return accumulatedCount / totalCount
}

export function validateNodesArgument(options: any): number {
  const nodes = parseInt(options.nodes, 10)
  if (isNaN(nodes)) {
    console.log('`--nodes` argument is not present or is not a number')
    process.exit(1)
  }
  return nodes
}

export function validateNumberOfSimulations(options: any): number {
  const simulations = parseInt(options.simulations, 10)
  if (isNaN(simulations)) {
    console.log('`--simulations` argument is not present or is not a number')
    process.exit(1)
  }
  return simulations
}

export function validateMaliciousNodesArguments(options: any): number {
  const maliciousCount = parseInt(options.malicious, 10)
  if (isNaN(maliciousCount)) {
    console.log('`--malicious` argument is not present or is not a number')
    process.exit(1)
  }
  return maliciousCount
}

export function validateVoteCountArguments(options: any): number {
  const votes = parseInt(options.votes, 10)
  if (isNaN(votes)) {
    console.log('`--votes` argument is not present or is not a number')
    process.exit(1)
  }
  return votes
}

export function validateFailureSizeArguments(options: any): number {
  const failurePercentage = parseInt(options.failure, 10)
  if (isNaN(failurePercentage)) {
    console.log('`--failure` argument is not present or is not a number')
    process.exit(1)
  }
  return failurePercentage
}

export function validateShardSizeArgument(options: any): number {
  const shardSize = parseInt(options.shard, 10)
  if (isNaN(shardSize)) {
    console.log('`--shard` argument is not present or is not a number')
    process.exit(1)
  }
  return shardSize
}

export function validateHonestNodesArgument(options: any): number {
  const honestNodes = parseInt(options.honest, 10)
  if (isNaN(honestNodes)) {
    console.log('`--honest` argument is not present or is not a number')
    process.exit(1)
  }
  return honestNodes
}

export function validateRotateNodesArgument(options: any): number {
  const rotateNodes = parseInt(options.rotate, 10)
  if (isNaN(rotateNodes)) {
    console.log('`--rotate` argument is not present or is not a number')
    process.exit(1)
  }
  return rotateNodes
}

export function validateProgressCountArgument(options: any): number {
  const progressCount = parseInt(options.progress, 10)
  if (isNaN(progressCount)) {
    console.log('`--progress` argument is not present or is not a number')
    process.exit(1)
  }
  return progressCount
}
