class ShardNode {
  nodeId: number
  joined: number
  honest: boolean = true
  startPosition: number = 0
  leftPosition: number = 0
  rightPosition: number = 0
  static bucketCount: number = 9
  static maxId: number = 10 ** 15

  constructor(joinTime: number, isHonest: boolean) {
    this.joined = joinTime
    this.honest = isHonest
    this.nodeId = Math.floor(Math.random() * ShardNode.maxId)
  }

  // update the node's position in the network
  updatePosition(updateTime: number, position: number): void {
    if (this.joined >= updateTime) {
      this.startPosition = position
      this.leftPosition = position
      this.rightPosition = position
    } else if (position < this.leftPosition) {
      this.leftPosition = position
    } else if (position > this.rightPosition) {
      this.rightPosition = position
    }
  }

  // provide the statistics for the node's drift
  getDrift(): number {
    return this.rightPosition - this.leftPosition
  }

  getBucket(): number {
    return Math.floor(this.nodeId / (ShardNode.maxId / ShardNode.bucketCount))
  }
}

export default ShardNode
