import ShardNode from './shardNode'

class NodeList {
  nodes: ShardNode[]
  networkSize: number
  shardSize: number
  failureSize: number
  totalCount: number
  maliciousCount: number
  maliciousInNetwork: number = 0
  nodeTime: number = 0
  rotationCount: number = 0
  cumulativeCounts: number[] = []
  maxMaliciousInShard: number = 0
  maliciousShards: number = 0
  statisticsTime: number = 0
  expiredNodes: ShardNode[] = []
  averageDrift: number[] = []
  maxDrift: number[] = []

  constructor(
    networkSize: number,
    shardSize: number,
    failureSize: number,
    maliciousCount: number,
    totalCount: number
  ) {
    this.nodes = []
    this.networkSize = networkSize
    this.shardSize = shardSize
    this.failureSize = failureSize
    this.totalCount = totalCount
    this.maliciousCount = maliciousCount

    // build the initial network
    while (this.nodeTime < networkSize) {
      let isHonest = Math.random() > (maliciousCount - this.maliciousInNetwork) / (totalCount - this.nodeTime)
      let node = new ShardNode(this.nodeTime, isHonest)
      this.nodes.push(node)

      if (!isHonest) {
        this.maliciousInNetwork += 1
      }

      this.nodeTime += 1
    }

    this.sortAndUpdatePositions(0)
  }

  computeCumulativeCounts(): void {
    this.cumulativeCounts = Array<number>(this.networkSize).fill(0)

    let currentSum = 0

    for (let i = 0; i < this.shardSize; i++) {
      if (!this.nodes[i].honest) {
        currentSum += 1
      }
    }

    for (let i = 0; i < this.networkSize - this.shardSize; i++) {
      this.cumulativeCounts[i] = currentSum

      if (!this.nodes[i].honest) {
        currentSum -= 1
      }

      if (!this.nodes[i + this.shardSize].honest) {
        currentSum += 1
      }
    }

    for (let i = this.networkSize - this.shardSize; i < this.networkSize; i++) {
      this.cumulativeCounts[i] = currentSum

      if (!this.nodes[i].honest) {
        currentSum -= 1
      }

      if (!this.nodes[i + this.shardSize - this.networkSize].honest) {
        currentSum += 1
      }
    }
  }

  countMaliciousShards(): void {
    this.maxMaliciousInShard = 0
    this.maliciousShards = 0

    this.cumulativeCounts.forEach((count) => {
      if (count > this.maxMaliciousInShard) {
        this.maxMaliciousInShard = count
      }

      if (count >= this.failureSize) {
        this.maliciousShards += 1
      }
    })
  }

  computeMaliciousShardStatistics(): void {
    if (this.statisticsTime < this.nodeTime) {
      this.computeCumulativeCounts()
      this.countMaliciousShards()

      this.statisticsTime = this.nodeTime
    }
  }

  cycleRotation(numberToRotate: number): void {
    this.rotationCount += numberToRotate

    for (let i = 0; i < this.networkSize; i++) {
      if (this.nodes[i].joined < this.rotationCount) {
        // remove the node from the network and add to expired nodes list
        this.expiredNodes.push(this.nodes[i])

        if (!this.nodes[i].honest) {
          this.maliciousInNetwork -= 1
        }
        let isHonest =
          Math.random() >
          (this.maliciousCount - this.maliciousInNetwork) / (this.totalCount - this.networkSize + 1)

        this.nodes[i] = new ShardNode(this.nodeTime, isHonest)

        this.nodeTime += 1

        if (!isHonest) {
          this.maliciousInNetwork += 1
        }
      }
    }

    this.sortAndUpdatePositions(this.nodeTime - numberToRotate)
  }

  sortAndUpdatePositions(updateTime: number): void {
    // sort the nodes by node id
    this.nodes.sort((a, b) => {
      return a.nodeId - b.nodeId
    })

    // update the positions of the nodes
    for (let i = 0; i < this.networkSize; i++) {
      this.nodes[i].updatePosition(updateTime, i)
    }
  }

  getMaliciousShardCount(): number {
    this.computeMaliciousShardStatistics()

    return this.maliciousShards
  }

  getMaxMaliciousInShard(): number {
    this.computeMaliciousShardStatistics()

    return this.maxMaliciousInShard
  }

  computeNodeDriftStats(): void {
    this.averageDrift = Array<number>(ShardNode.bucketCount).fill(0)
    this.maxDrift = Array<number>(ShardNode.bucketCount).fill(0)
    let buckets = Array<number>(ShardNode.bucketCount).fill(0)

    for (let i = this.networkSize; i < this.expiredNodes.length; i++) {
      let bucket = this.expiredNodes[i].getBucket()
      let drift = this.expiredNodes[i].getDrift()

      this.averageDrift[bucket] += drift
      buckets[bucket] += 1
      if (drift > this.maxDrift[bucket]) {
        this.maxDrift[bucket] = drift
      }
    }

    for (let i = 0; i < ShardNode.bucketCount; i++) {
      if (buckets[i] > 0) {
        this.averageDrift[i] /= buckets[i]
      }
    }
  }

  getVoteScore(voteCount: number): number {
    let score = 0

    // take the top voteCount nodes and sum up their node IDs
    for (let i = 0; i < voteCount; i++) {
      score += this.nodes[i].nodeId
    }

    return score
  }
}

export default NodeList
