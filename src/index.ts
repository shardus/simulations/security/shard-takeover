import { TestMaliciousShard } from './cmd/malicious-shard'
import { TestStandbyAttack } from './cmd/standby-attack'
import { TestFailureTime } from './cmd/failure-time'
import { TestNodeDrift } from './cmd/node-drift'
import { TestChallengeCover } from './cmd/challenge-cover'
import { TestCoverRotation } from './cmd/cover-rotation'
import { TestCycleMarker } from './cmd/cycle-marker'
const program = require('commander')

/*
 * `malicious-shard` command is used to simulate random shard assignment given
 * the presence of a significant proportion of malicious nodes, to see if a
 * shard might wind up with a majority of malicious nodes.  This checks all
 * possible shards made up of consecutive nodes (placed in a circle) after
 * randomly assigning a fixed percentage of nodes to be malicious, so the total
 * number of checked shards is equal to the number of nodes.
 *
 * It also calculates the theoretical probability for a shard to have a majority
 * of malicious nodes in two ways.  First, it calculates this when each node is
 * indepedently malicious with the given percentage odds (this is independent of
 * the number of nodes).  It also calculates this when the total number of
 * malicious nodes is fixed, as specified by the percentage and the total number
 * of nodes.
 *
 * The program takes 7 arguments:
 *
 * -n - the number of nodes to simulate
 * -s - the shard size
 * -h - the number of honest nodes
 * -m - the number of malicious nodes
 * -f - the number of malicious nodes in a shard that indicates failure
 * -t - the number of simulations to perform
 * -p - the number of times to print a progress bar
 *
 * An example run of the program looks like this:
 * `node build/src/index.js malicious-shard -n 1000 -s 128 -h 3300 -m 1700 -f 65 -t 100000`
 */
program
  .command('malicious-shard')
  .description('Simulates shard formation with a given percentage of malicious nodes')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-h, --honest <number>', 'number of honest nodes', 1000)
  .option('-m, --malicious <number>', 'number of malicious nodes', 330)
  .option('-f, --failure <number>', 'number of malicious nodes in a shard that indicates failure', 65)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestMaliciousShard(options)
  })

/*
 * `failure-time` command is used to simulate an attacker that is able to
 * control a certain percentage of nodes in the network, where we run a simulation
 * until a shard fails, and then we record the number of rotations it took.
 *
 * The program takes 8 arguments:
 *
 * -n - the number of active nodes
 * -s - the shard size
 * -h - the number of honest nodes
 * -m - the number of malicious nodes
 * -f - the number of malicious nodes in a shard that indicates failure
 * -t - the total amount of time to run simulations
 * -r - the number of nodes to rotate out on each cycle
 * -p - the number of times to print a progress bar
 *
 * An example run of the program looks like this:
 * `node build/src/index.js failure-time -n 1000 -s 128 -h 5000 -m 2500 -f 65 -r 2 -t 100000`
 */
program
  .command('failure-time')
  .description('Simulates a specific attack with a given number of malicious nodes')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-h, --honest <number>', 'number of honest nodes', 4000)
  .option('-m, --malicious <number>', 'number malicious nodes', 2000)
  .option('-f, --failure <number>', 'number of malicious nodes in a shard that indicates failure', 65)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-r, --rotate <number>', 'number of nodes to rotate out on each cycle', 2)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestFailureTime(options)
  })

/*
 * `standby-attack` command is used to simulate an attacker that is able to
 * control a certain percentage of nodes on standby, which gives them a
 * greater percentage of joining the network, due to the deterministic
 * nature of the rotation process.
 *
 * A single simulation here runs until the number of rotations is double
 * the size of the network, which should be enough to maximize the chances
 * of the attacker getting a majority of nodes in a shard.
 *
 * The program takes 8 arguments:
 *
 * -n - the number of active nodes
 * -s - the shard size
 * -h - the number of honest nodes
 * -m - the number of malicious nodes added to the standby list
 * -f - the percentage of nodes in a shard that indicates failure
 * -t - the number of simulations to perform
 * -r - the number of nodes to rotate out on each cycle
 * -p - the number of times to print a progress bar
 *
 * An example run of the program looks like this:
 * `node build/src/index.js standby-attack -n 1000 -s 128 -h 5000 -m 2500 -f 65 -r 2 -t 50`
 */
program
  .command('standby-attack')
  .description('Simulates a specific attack with a given number of malicious nodes')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-h, --honest <number>', 'number of honest nodes', 4000)
  .option('-m, --malicious <number>', 'number malicious nodes added to standby', 2000)
  .option('-f, --failure <number>', 'number of malicious nodes in a shard that indicates failure', 65)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-r, --rotate <number>', 'number of nodes to rotate out on each cycle', 2)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestStandbyAttack(options)
  })

/*
 * `node-drift` command is used to simulate the drift of nodes in the network.
 * This is done by simulating the rotation of nodes in the network, and
 * recording how much the position of each node has changed.
 *
 * A single simulation here runs until the number of rotations specified has
 * been performed, and the statistics are recorded in several buckets.
 *
 * The program takes 5 arguments:
 *
 * -n - the number of active nodes
 * -s - the shard size
 * -t - the number of simulations to perform
 * -r - the number of nodes to rotate out on each cycle
 * -p - the number of times to print a progress bar
 *
 * An example run of the program looks like this:
 * `node build/src/index.js node-drift -n 1000 -s 128 -r 2 -t 10000`
 */
program
  .command('node-drift')
  .description('Simulates the drift of nodes in the network')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-r, --rotate <number>', 'number of nodes to rotate out on each cycle', 2)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestNodeDrift(options)
  })

/*
 * 'challenge-cover' command simulates random shard formation given a certain percentage of malicious
 * nodes, to see if each shard has any malicious nodes.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * -n - the number of active nodes
 * -s - the number of nodes per shard
 * -h - the number of honest nodes
 * -m - the number of malicious nodes
 * -f - the number of shards with malicious nodes that constitutes failure
 * -t - the number of times to run the simulation
 * -p - the number of progress updates to show while running the simulation
 *
 * An example run of the program looks like this:
 * `node build/src/index.js challenge-cover -n 1000 -s 128 -h 4000 -m 100 -f 100 -t 10000`
 */
program
  .command('challenge-cover')
  .description('Simulates shard coverage with a given number of malicious nodes')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-h, --honest <number>', 'number of external nodes', 4000)
  .option('-m, --malicious <number>', 'number of malicious nodes', 100)
  .option('-f, --failure <number>', 'number of shards with malicious nodes that indicates failure', 1000)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestChallengeCover(options)
  })

/*
 * 'cover-rotation' simulates random shard formation given a certain percentage of malicious
 * nodes, to see how many shards have a malicious node.  Then, it runs rotation for the
 * specified number of cycles and collects statistics.
 *
 * @param {any} - options, the commander data structure which has all the parsed arguments.
 * Options should have the following parameters that are extracted and used in this function.
 *
 * -n - the number of active nodes
 * -s - the number of nodes per shard
 * -h - the number of honest nodes
 * -m - the number of malicious nodes
 * -f - the number of shards with malicious nodes that constitutes failure
 * -t - the number of times to run the simulation
 * -r - the number of nodes to rotate out on each cycle
 * -p - the number of progress updates to show while running the simulation
 *
 * An example run of the program looks like this:
 * `node build/src/index.js cover-rotation -n 1000 -s 128 -h 4000 -m 100 -f 1000 -r 2 -t 100000`
 */
program
  .command('cover-rotation')
  .description('Simulates rotation with attacker maintaining malicious nodes in shards')
  .option('-n, --nodes <number>', 'number of active nodes', 1000)
  .option('-s, --shard <number>', 'number of nodes in a consensus group', 128)
  .option('-h, --honest <number>', 'number of honest nodes', 4000)
  .option('-m, --malicious <number>', 'number malicious nodes', 2000)
  .option('-f, --failure <number>', 'number of shard with malicious nodes that indicates failure', 1000)
  .option('-t, --simulations <number>', 'number of simulations to perform', 100)
  .option('-r, --rotate <number>', 'number of nodes to rotate out on each cycle', 2)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestCoverRotation(options)
  })

/*
 * 'cycle-marker' simulates malicious nodes capable of trying different cycle markers so as to
 * get high values when hashed with several malicious node IDs. It will compute different
 * relevant statistics.
 *
 * The program takes 6 arguments:
 *
 * -n - the number of active nodes
 * -m - the number of active malicious nodes
 * -v - the number of votes needed for a marker to be a viable candidate
 * -h - the number of active honest node votes to generate
 * -t - the total number of forging simulations to run
 * -p - the number of progress updates to show while running the simulation
 *
 * An example run of the program looks like this:
 * `node build/src/index.js cycle-marker -n 1000 -m 50 -v 3 -h 10 -t 10000`
 */
program
  .command('cycle-marker')
  .description('Simulates cycle marker voting with attacker precomputing hashes')
  .option('-n, --nodes <number>', 'number of nodes in the network', 1000)
  .option('-m, --malicious <number>', 'number malicious nodes in the network', 50)
  .option('-v, --votes <number>', 'number of votes retained for a cycle marker', 3)
  .option('-h, --honest <number>', 'number of active honest node votes to generate', 10)
  .option('-t, --simulations <number>', 'number of forging simulations to perform', 1000)
  .option('-p, --progress <number>', 'number of times to print a progress bar', 10)
  .action((options) => {
    TestCycleMarker(options)
  })

program.parse()
